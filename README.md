### LEOCODE RECRUITMENT TASK

This is sample solution for Leocode Recruitment Task

### Setup 

Copy and set proper values: 

```
.env.example --> .env
```

Run: 

```
yarn install 

yarn build

yarn start
```


Run E2E Tets:

```
yarn test
```

### Endpoints: 

### Signin

Used to authorize user

**URL** : `/api/sign-in`

**Method** : `POST`

**Auth required** : x-api-token, Authorization Bearer

**Data constraints**

```json
{
  "email" : "example@mail.com" ,
  "password" : "1234"
}

```


## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "authToken" : "eyJhbGc..."
}

```


### Generate Key Pair

Used to generate RSA Keys

**URL** : `/api/generate-key-pair`

**Method** : `POST`

**Auth required** : Authorization

**Data constraints**



## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "privKey": "-----BEGIN ENCRYPTED PRIVATE KEY----- MIIJrT ….",
  "pubKey": "-----BEGIN PUBLIC KEY-----MII…"
}


```

### Generate Key Pair

Used for encode sample image

**URL** : `/api/encrypt`

**Method** : `POST`

**Auth required** : Authorization

**Data constraints**



## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "encryptedString": "ddfFdas9ada...."
}


```
