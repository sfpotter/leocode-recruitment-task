import {ResponseTypes, RouterTypes} from '@interfaces';
import * as Router from 'koa-router';
import {StatusCodes} from 'http-status-codes';


const buildResponse = <T>(status = StatusCodes.OK, dataProcessor?: (data: T) => any) => (data?: T): ResponseTypes.Response<any> => ({
    status,
    ...(data && {body: dataProcessor ? dataProcessor(data) : data})
});

const buildRoutings = (routes: RouterTypes.RoutesDescriptor, router: Router): Router => {
    const addPathPart = (path: string, part: string): string => `${path}${part !== '/' ? `/${part}` : ''}`;
    const traverse = (obj: any, path: string): void => {
        Object.entries(obj)
            .map(entry => ({key: entry[0] as any, value: entry[1] as any}))
            .forEach(tuple => {
                if (tuple.value.constructor === Object && !(tuple.value instanceof Function)) {
                    traverse(tuple.value, addPathPart(path, tuple.key));
                } else {
                    const r = tuple.value();
                    router.use(addPathPart(path, tuple.key), r.routes(), r.allowedMethods());
                }
            });
    };

    traverse(routes, '');

    return router;
};

export {
    buildRoutings,
    buildResponse
}
