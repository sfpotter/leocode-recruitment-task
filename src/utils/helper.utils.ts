const defaultTo = (value: any, defaultValue: any) => (value == null || value !== value) ? defaultValue : value;

export {
    defaultTo
}
