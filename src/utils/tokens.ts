import * as jwt from 'jsonwebtoken';
import { randomBytes, generateKeyPairSync, createSign } from 'crypto';

/**
 * Generate a random token string
 */
export const generateRandomToken = (length = 43): string => randomBytes(length).toString('hex');

export const generateAccessToken = ({
                                        secret,
                                        payload = {},
                                        config,
                                    }: {
    secret: jwt.Secret;
    payload?: any;
    config: jwt.SignOptions;
}) => jwt.sign(payload, secret, config);


export const generateRsaKeyPair = () => {
    const { publicKey, privateKey } = generateKeyPairSync('rsa', {
        modulusLength: 2048,
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem',
            cipher: 'aes-256-cbc',
            passphrase: 'passphrase'
        }
    })

    return {
        publicKey,
        privateKey
    }
};
