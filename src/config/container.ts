import { Container } from 'inversify';
import {AuthorizationApiService} from '@services/api/authorization.api.service';
import {TYPES} from './types.config';
import {SignIn} from '@handlers/internal/v1/signin';
import {UserMapperService} from '@services/mapper/user.mapper.service';
import {AuthorizationService} from '@services/authorization.service';
import {TokenMapperService} from '@services/mapper/token.mapper.service';
import {KeysHandler} from '@handlers/internal/v1/keys';
import {EncryptMapperService} from '@services/mapper/encrypt.mapper.service';
import {EncryptApiService} from '@services/api/encrypt.api.service';
import {EncryptService} from '@services/encrypt.service';
import {EncryptHandler} from '@handlers/internal/v1/encrypt';

const appContainer: Container = new Container();

const setupBindings = (container: Container): void => {
    container.bind<SignIn>(TYPES.SignInHandler).to(SignIn);
    container.bind<KeysHandler>(TYPES.KeysHandler).to(KeysHandler);

    container.bind<TokenMapperService>(TYPES.TokenMapperService).to(TokenMapperService)
    container.bind<UserMapperService>(TYPES.FilmsMapperService).to(UserMapperService);
    container.bind<AuthorizationApiService>(TYPES.AuthorizationApiService).to(AuthorizationApiService);
    container.bind<AuthorizationService>(TYPES.AuthorizationService).to(AuthorizationService).inSingletonScope()
    container.bind<EncryptMapperService>(TYPES.EncryptMapper).to(EncryptMapperService);
    container.bind<EncryptApiService>(TYPES.EncryptApiService).to(EncryptApiService);
    container.bind<EncryptService>(TYPES.EncryptService).to(EncryptService);
    container.bind<EncryptHandler>(TYPES.EncryptHandler).to(EncryptHandler);

}

const initIocContainer = (): void => setupBindings(appContainer);

export { appContainer, initIocContainer };
