import * as Router from 'koa-router';
import {RouterTypes} from '@interfaces';
import {HandlerUtils} from '@utils';
import signIn from '@handlers/internal/v1/signin/routes'
import keys from '@handlers/internal/v1/keys/routes'
import encrypt from '@handlers/internal/v1/encrypt/routes'
const publicRoutes: string[] = ['/api/sign-in'];


const routes: RouterTypes.RoutesDescriptor = {
    api: {
        'sign-in': signIn,
        'generate-key-pair': keys,
        encrypt
    },
};

const buildRoutes = (): Router.IMiddleware => HandlerUtils.buildRoutings(routes, new Router()).routes();

export {
    buildRoutes,
    publicRoutes
};
