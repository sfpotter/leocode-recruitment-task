import * as winston from 'winston';
import {Logger} from 'winston';
import * as logform from 'logform';
import {serializeError} from 'serialize-error';

import {config} from '@config';
import {AppStageEnum} from '@enums';

const LOGGER: Logger = winston.createLogger();

const configureLogger = (hostname: string) => {
    const jsonFormat: logform.Format = winston.format.combine(
        winston.format.timestamp(),
        winston.format.metadata(),
        winston.format.json({
            replacer: (key, value) => {
                if (key === 'metadata' && value.error) {
                    value.error = serializeError(value.error);
                }

                return value;
            }
        })
    );

    LOGGER.configure({
        level: config.stage !== AppStageEnum.Production ? 'debug' : 'info',
        transports: [
            new winston.transports.Console({
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.timestamp(),
                    winston.format.metadata(),
                    winston.format.printf(({level, message, metadata}: logform.TransformableInfo) => {
                        const {timestamp, ...meta} = metadata;
                        const formatted = `${timestamp} [${level}]: ${message}`;

                        if (meta.constructor === Object) {
                            return formatted;
                        }

                        return `${formatted} - ${JSON.stringify(meta)}`;
                    })
                )
            })
        ]
    });
};

export {
    configureLogger,
    LOGGER
};
