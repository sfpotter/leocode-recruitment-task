const TYPES = {
    SignInHandler: Symbol.for('SignInHandler'),
    KeysHandler: Symbol.for('KeysHandler'),

    AuthorizationApiService: Symbol.for('FilmsApiService'),
    FilmsMapperService: Symbol.for('FilmsMapperService'),
    TokenMapperService: Symbol.for('TokenMapperService'),
    AuthorizationService: Symbol.for('FilmsBeEsService'),
    EncryptMapper: Symbol.for('EncryptMapper'),
    EncryptService: Symbol.for('EncryptService'),
    EncryptApiService: Symbol.for('EncryptApiService'),
    EncryptHandler: Symbol.for('EncryptHandler'),
}

export { TYPES };
