import {HelperUtils} from '@utils';
import * as dotenv from 'dotenv';
import {AppStageEnum} from '@enums';

dotenv.config({ path: '.env' });


export interface IConfig {
    stage: AppStageEnum;
    port: number;
}

const stage: AppStageEnum = HelperUtils.defaultTo(process.env.STAGE, AppStageEnum.Local);

export const config: IConfig = {
    stage,
    port: HelperUtils.defaultTo(+process.env.PORT, 3000),
};
