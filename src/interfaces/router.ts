import * as Router from 'koa-router';

interface RoutesDescriptor {
    [key: string]: RoutesDescriptor | (() => Router);
}

export {
    RoutesDescriptor
};
