import {ExtendableContext} from 'koa';

interface Context extends ExtendableContext {
    state: any;
    params: any;
    query: any;
}

type NextMiddleware = () => Promise<void>;

export {
    Context,
    NextMiddleware
};
