interface Response<T> {
    status: number;
    body: T;
}

interface PaginatedEsResponse<T> {
    status: number;
    body: T[];
    totalCount: number;
}

interface PaginatedIntegrationSyncResponse<T> {
    status: number;
    body: T[];
    paginationToken: string;
}

interface PaginatedNotificationResponse<T> {
    status: number;
    body: T[];
    hasNextPage: boolean;
}

export {
    Response,
    PaginatedEsResponse,
    PaginatedIntegrationSyncResponse,
    PaginatedNotificationResponse
};
