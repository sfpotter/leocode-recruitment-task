import 'reflect-metadata';
import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as cors from '@koa/cors';
import * as helmet from 'koa-helmet';
import {appContainer, buildRoutes, configureLogger, initIocContainer, TYPES} from '@config';
import {GeneralMw} from '@middlewares';
import {RequestTypes} from '@interfaces';
import {AuthorizationService} from '@services/authorization.service';
import * as os from 'os';

const bootstrap = (): Koa => {
    const app: Koa = new Koa();
    app.use(cors({ credentials: true, origin: '*' }));
    app.use(GeneralMw.requestLogging);
    app.use(GeneralMw.requestErrorHandling);
    app.use(async (ctx: RequestTypes.Context, next: RequestTypes.NextMiddleware) => {
        return GeneralMw.authenticated(ctx, next, appContainer.get<AuthorizationService>(TYPES.AuthorizationService))
    });
    app.use(helmet());
    app.use(bodyParser());
    app.use(buildRoutes());

    return app;
};

const server = async () => {
    await initIocContainer();
    configureLogger(os.hostname());

    console.log(`LISTENING SERVER ON port ${process.env.NODE_PORT}`)
    return bootstrap().listen(process.env.NODE_PORT);
}

(async () => {
   await server();
})()
