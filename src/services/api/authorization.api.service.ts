import {inject, injectable} from 'inversify';

import {TYPES} from '@config';
import {AbstractApiService} from '@services/api/api.abstract.service';
import {AuthorizationService} from '@services/authorization.service';
import { UserDtos } from '@dtos';
import {TokenDtos} from '@dtos';
import {TokenMapperService} from '@services/mapper/token.mapper.service';

@injectable()
export class AuthorizationApiService extends AbstractApiService<TokenMapperService> {
    constructor(@inject(TYPES.AuthorizationService) private authorizationService: AuthorizationService,
                @inject(TYPES.TokenMapperService) tokenMapperService: TokenMapperService) {
        super(tokenMapperService);
    }

    public async login(body: UserDtos.User): Promise<TokenDtos.AuthToken> {
        return this.mapper.toResponse(
            await this.authorizationService.loginUser(body)
        );
    }

    public async generateRsaKey(owner: string): Promise<any> {
        return this.mapper.toRsaKeyDto(
            this.authorizationService.generateRsa(owner)
        )
    }
}
