import {inject, injectable} from 'inversify';

import {TYPES} from '@config';
import {AbstractApiService} from '@services/api/api.abstract.service';
import {AuthorizationService} from '@services/authorization.service';
import { UserDtos } from '@dtos';
import {TokenDtos} from '@dtos';
import { EncryptMapperService } from '@services/mapper/encrypt.mapper.service';
import {EncryptService} from '@services/encrypt.service';

@injectable()
export class EncryptApiService extends AbstractApiService<EncryptMapperService> {
    constructor(@inject(TYPES.AuthorizationService) private authorizationService: AuthorizationService,
                @inject(TYPES.EncryptService) private encryptService: EncryptService,
                @inject(TYPES.EncryptMapper) mapper: EncryptMapperService) {
        super(mapper);
    }

    public async encryptImage(owner: string) {
        return this.mapper.toResponse(await this.encryptService.encryptSampleImage(owner));
    }
}
