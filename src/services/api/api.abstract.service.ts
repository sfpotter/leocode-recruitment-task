import {injectable} from 'inversify';

import {AbstractMapperService} from '@services/mapper/mapper.abstract.service';

@injectable()
export abstract class AbstractApiService<T extends AbstractMapperService> {
    protected constructor(private mapperService: T) {
    }

    public get mapper(): T {
        return this.mapperService;
    }
}
