import {injectable} from 'inversify';
import {AbstractMapperService} from '@services/mapper/mapper.abstract.service';
import { ImageDtos } from '@dtos';

@injectable()
export class EncryptMapperService extends AbstractMapperService {
    public toResponse(item: ImageDtos.Image) {
        return item;
    }
}
