import {injectable} from 'inversify';
import {AbstractMapperService} from '@services/mapper/mapper.abstract.service';
import { UserDtos } from '@dtos';

@injectable()
export class UserMapperService extends AbstractMapperService {
    public toResponse(item: UserDtos.User) {
        return item;
    }
}
