import {injectable} from 'inversify';
import {AbstractMapperService} from '@services/mapper/mapper.abstract.service';
import { TokenDtos } from '@dtos';
import * as crypto from 'crypto';

@injectable()
export class TokenMapperService extends AbstractMapperService {
    public toResponse(item: TokenDtos.AuthToken) {
        return item;
    }

    public toRsaKeyDto(data: TokenDtos.Rsa): TokenDtos.Rsa {
        return data;
    }
}
