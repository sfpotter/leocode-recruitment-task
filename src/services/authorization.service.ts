import {injectable} from 'inversify';

import {UserDtos} from '@dtos';
import {UnauthorizedError} from '../errors/unauthorized';
import {randomBytes} from 'crypto';
import {TokenDtos} from '@dtos';
import {generateAccessToken, generateRsaKeyPair} from '../utils/tokens';
import * as jwt from 'jsonwebtoken';
import {ApplicationError} from '@errors';


@injectable()
export class AuthorizationService {
    private sessionsTokens: UserDtos.UserAuthenticatedTokens[] = [];
    private samplesUsers: UserDtos.User[] = [
        { id: 1, email: 'piotr@gmail.com', password: 'sample' } as UserDtos.User,
        { id:2,  email: 'sample@gmail.com', password: 'sample123' } as UserDtos.User
    ];

    private TOKEN_EXPIRATION = process.env.TOKEN_EXPIRATION;
    private TOKEN_SECRET = process.env.TOKEN_SECRET;

    private static generateSessionToken(length = 43): string {
        return randomBytes(length).toString('hex')
    }

    public loginUser(body: UserDtos.User) : TokenDtos.AuthToken {
        if (!this.TOKEN_SECRET || !this.TOKEN_EXPIRATION) throw new ApplicationError('TOKEN EXPIRATION OR TOKEN SECRET IS NOT SET');

        const findUser = this.samplesUsers.filter(
            (data) => data.email === body.email && data.password === body.password
        )
        if (findUser.length === 0) throw new UnauthorizedError('Invalid credentials');

        const jwtData = {
            token: AuthorizationService.generateSessionToken(),
            isImpersonated: true,
            email: findUser[0].email
        }

        const token =  {
            authToken: generateAccessToken({
                payload: jwtData,
                secret: this.TOKEN_SECRET,
                config: {
                    expiresIn: this.TOKEN_EXPIRATION
                }
            })
        } as TokenDtos.AuthToken;

        this.sessionsTokens.push({
            user: findUser[0],
            token: token.authToken
        });

        return token;
    }


    public resumeSession(token: string): UserDtos.UserAuthenticatedTokens {
        if (!token) throw new UnauthorizedError('Unauthorized');

        const verifyToken = jwt.verify(token, this.TOKEN_SECRET);
        const findSavedSessions = this.sessionsTokens.filter((data) => data.token === token);

        if (verifyToken && findSavedSessions.length > 0) {
            return findSavedSessions[0];
        }

        throw new UnauthorizedError('Unauthorized')
    }

    private getByOwner(owner: string) {
        return this.samplesUsers.filter(
            (data) => data.id === Number(owner)
        )[0]
    }


    public generateRsa(owner: string) {
        const user = this.getByOwner(owner);

        const keys = generateRsaKeyPair();
        user.keys = keys;

        return keys;
    }
}
