import {inject, injectable} from 'inversify';
import {ImageDtos} from '@dtos';
import * as request from 'request';
import {TYPES} from '@config';
import * as NodeRsa from 'node-rsa';
import {BadRequestError} from '@errors';

@injectable()
export class EncryptService {
    private SAMPLE_URL = 'http://www.africau.edu/images/default/sample.pdf';

    constructor(@inject(TYPES.AuthorizationService) private authorizationService) {
    }


    public async encryptSampleImage(owner: string): Promise<ImageDtos.Image> {
       const user = this.authorizationService.getByOwner(owner);

       if (!user.keys || !user.keys.publicKey) throw new BadRequestError('Cannot find user private key');

       const encryptedString = await (() => new Promise(((resolve, reject) => {
            request.get(this.SAMPLE_URL, (err, res, body) => {
                if (err) reject(err);

                const key = new NodeRsa(user.keys.publicKey);
                resolve(key.encrypt(body, 'base64'));
           })
       })))() as string;



       return {
           encryptedString
       }
    }
}
