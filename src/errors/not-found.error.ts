import {StatusCodes} from 'http-status-codes';

import {ApplicationError} from '.';

export class NotFoundError extends ApplicationError {
  constructor(message?: string, payload?: any, cause?: Error) {
    super(message ?? 'Entity is not found', payload, StatusCodes.NOT_FOUND, cause);
    this.name = 'NotFoundError';

    Object.setPrototypeOf(this, new.target.prototype);
  }
}
