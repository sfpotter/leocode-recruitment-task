import {StatusCodes} from 'http-status-codes';

export class ApplicationError extends Error {
    private payload: any;
    private status: number;

    constructor(message?: string, payload?: any, status = StatusCodes.INTERNAL_SERVER_ERROR, cause?: Error) {
        super(message);
        this.name = 'ApplicationError';
        this.status = status;
        this.payload = payload;

        if (cause) {
            const causePayload: any = cause instanceof ApplicationError ? cause?.payload : {};

            this.stack += `\nCaused by: ${cause.name} <${JSON.stringify(causePayload)}> ${cause.stack}`;
        }

        Object.setPrototypeOf(this, new.target.prototype);
    }
}
