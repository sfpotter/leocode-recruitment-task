import {StatusCodes} from 'http-status-codes';

import {ApplicationError} from '.';

export class UnauthorizedError extends ApplicationError {
    constructor(message?: string, payload?: any, cause?: Error) {
        super(message ?? 'Bad request', payload, StatusCodes.UNAUTHORIZED, cause);
        this.name = 'BadRequestError';

        Object.setPrototypeOf(this, new.target.prototype);
    }
}
