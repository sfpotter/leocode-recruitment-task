import {StatusCodes} from 'http-status-codes';

import {ApplicationError} from '.';

export class ForbiddenError extends ApplicationError {
  constructor(message?: string, payload?: any, cause?: Error) {
    super(message ?? 'You are not authorized to access this data', payload, StatusCodes.FORBIDDEN, cause);
    this.name = 'ForbiddenError';

    Object.setPrototypeOf(this, new.target.prototype);
  }
}
