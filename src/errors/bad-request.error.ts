import {StatusCodes} from 'http-status-codes';

import {ApplicationError} from '.';

export class BadRequestError extends ApplicationError {
    constructor(message?: string, payload?: any, cause?: Error) {
        super(message ?? 'Bad request', payload, StatusCodes.BAD_REQUEST, cause);
        this.name = 'BadRequestError';

        Object.setPrototypeOf(this, new.target.prototype);
    }
}
