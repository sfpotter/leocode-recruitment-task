class AuthToken {
    authToken: string
}

class Rsa {
    publicKey: string;
    privateKey: string;
}

export {
    AuthToken,
    Rsa
};
