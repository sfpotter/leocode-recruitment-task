import {Allow, IsNotEmpty} from 'class-validator';
import {Rsa} from './token.dto';

class User {
    @Allow()
    id: number;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    password: string;

    keys: Rsa;
}


class UserAuthenticatedTokens {
    user: User;
    token: string;
}

export {
    User,
    UserAuthenticatedTokens
};
