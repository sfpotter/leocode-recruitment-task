import { expect } from 'chai';
import server from '../server';
import * as supertest from 'supertest';

describe('E2E Api Test', () => {
    let request: supertest.SuperTest<supertest.Test>;
    let serverInstance;
    before(async () => {
        serverInstance = await server();
        request = supertest(serverInstance);
    })

    it ('should return JWT token', async () => {
        await request
            .post(`/api/sign-in`)
            .send({
                email: 'piotr@gmail.com',
                password: 'sample'
            })
            .then((res) => {
                expect(res.body.status).eq(200);
                expect(res.body.body).to.have.property('authToken');
            })
    });


    it ('should return RSA keys', async () => {
        await request
            .post(`/api/sign-in`)
            .send({
                email: 'piotr@gmail.com',
                password: 'sample'
            })
            .then(async (res) => {
                await request
                    .post(`/api/generate-key-pair`)
                    .set('Authorization', `${res.body.body.authToken}`)
                    .then((res1) => {
                        expect(res1.body.status).eq(200);
                        expect(res1.body.body).to.have.property('publicKey');
                        expect(res1.body.body).to.have.property('privateKey');
                    })
            })
    })


    it('should encrypt sample image', async () => {
        await request
            .post(`/api/sign-in`)
            .send({
                email: 'piotr@gmail.com',
                password: 'sample'
            })
            .then(async (res) => {
                const token = res.body.body.authToken;
                await request
                    .post(`/api/generate-key-pair`)
                    .set('Authorization', `${token}`)
                    .then(async () => {
                        await request
                            .post(`/api/encrypt`)
                            .set('Authorization', `${token}`)
                            .then((res1) => {
                                expect(res1.body.status).eq(200);
                                expect(res1.body.body).to.have.property('encryptedString');
                            })
                    })
            })
    })

    it ('should throw error if token is not provided', async () => {
        await request
            .post(`/api/generate-key-pair`)
            .then(async (res) => {
                expect(res.body.code).eq(401);
            })
    })

    after(() => {
        serverInstance.close();
    })
})
