import {inject, injectable} from 'inversify';
import {StatusCodes} from 'http-status-codes';

import {TYPES} from '@config';
import {HandlerUtils} from '@utils';
import {RequestTypes} from '@interfaces';
import {AuthorizationApiService} from '@services/api/authorization.api.service';
import {EncryptService} from '@services/encrypt.service';

@injectable()
export class KeysHandler {
    constructor(
        @inject(TYPES.AuthorizationApiService) private apiService: AuthorizationApiService,
        @inject(TYPES.EncryptService) private encryptService: EncryptService
    ) {
    }

    public async generate(ctx: RequestTypes.Context): Promise<void> {
        ctx.status = StatusCodes.OK;
        ctx.body = await this.apiService.generateRsaKey(ctx.state.owner)
            .then(HandlerUtils.buildResponse(StatusCodes.OK));
    }

    public async encrypt(ctx: RequestTypes.Context): Promise<void> {
        ctx.status = StatusCodes.OK;
        ctx.body = await this.encryptService.encryptSampleImage(ctx.state.owner)
            .then(HandlerUtils.buildResponse(StatusCodes.OK));
    }
}
