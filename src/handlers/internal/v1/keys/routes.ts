import * as Router from 'koa-router';

import {appContainer, TYPES} from '@config';
import {ValidatorMw} from '@middlewares';
import {TokenDtos} from '@dtos';
import {KeysHandler} from '@handlers/internal/v1/keys/handler';

export default () => {
    const router: Router = new Router();
    const handler: KeysHandler = appContainer.get<KeysHandler>(TYPES.KeysHandler);

    router
        .post(
            '/',
            ValidatorMw.validatedBodyByModel(TokenDtos.Rsa, {whitelist: true, forbidNonWhitelisted: true}),
            handler.generate.bind(handler)
        )

    return router;
};
