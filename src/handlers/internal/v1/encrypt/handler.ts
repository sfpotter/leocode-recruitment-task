import {inject, injectable} from 'inversify';
import {StatusCodes} from 'http-status-codes';

import {TYPES} from '@config';
import {HandlerUtils} from '@utils';
import {RequestTypes} from '@interfaces';
import {AuthorizationApiService} from '@services/api/authorization.api.service';
import {EncryptApiService} from '@services/api/encrypt.api.service';

@injectable()
export class EncryptHandler {
    constructor(
        @inject(TYPES.AuthorizationApiService) private apiService: AuthorizationApiService,
        @inject(TYPES.EncryptApiService) private encryptService: EncryptApiService
    ) {
    }

    public async encryptSampleImage(ctx: RequestTypes.Context): Promise<void> {
        ctx.status = StatusCodes.OK;
        ctx.body = await this.encryptService.encryptImage(ctx.state.owner)
            .then(HandlerUtils.buildResponse(StatusCodes.OK));
    }
}
