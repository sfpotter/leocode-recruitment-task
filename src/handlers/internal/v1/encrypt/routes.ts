import * as Router from 'koa-router';

import {appContainer, TYPES} from '@config';
import {ValidatorMw} from '@middlewares';
import {TokenDtos} from '@dtos';
import {EncryptHandler} from '@handlers/internal/v1/encrypt/handler';

export default () => {
    const router: Router = new Router();
    const handler: EncryptHandler = appContainer.get<EncryptHandler>(TYPES.EncryptHandler);

    router
        .post(
            '/',
            handler.encryptSampleImage.bind(handler)
        )

    return router;
};
