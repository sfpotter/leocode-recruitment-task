import * as Router from 'koa-router';

import {appContainer, TYPES} from '@config';
import {ValidatorMw} from '@middlewares';
import {UserDtos} from '@dtos';
import {SignIn} from '@handlers/internal/v1/signin/handler';

export default () => {
    const router: Router = new Router();
    const handler: SignIn = appContainer.get<SignIn>(TYPES.SignInHandler);

    router
        .post(
            '/',
            ValidatorMw.validatedBodyByModel(UserDtos.User, {whitelist: true, forbidNonWhitelisted: true}),
            handler.signIn.bind(handler)
        )

    return router;
};
