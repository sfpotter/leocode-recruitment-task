import {inject, injectable} from 'inversify';
import {StatusCodes} from 'http-status-codes';

import {TYPES} from '@config';
import {HandlerUtils} from '@utils';
import {RequestTypes} from '@interfaces';
import {AuthorizationApiService} from '@services/api/authorization.api.service';

@injectable()
export class SignIn {
    constructor(@inject(TYPES.AuthorizationApiService) private apiService: AuthorizationApiService) {
    }

    public async signIn(ctx: RequestTypes.Context): Promise<void> {
        ctx.status = StatusCodes.OK;
        ctx.body = await this.apiService.login(ctx.state.body)
            .then(HandlerUtils.buildResponse(StatusCodes.OK));
    }
}
