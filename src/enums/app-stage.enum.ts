export enum AppStageEnum {
    Local = 'local',
    Production = 'prod',
    Development = 'dev',
}
