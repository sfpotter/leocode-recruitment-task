import {validate, ValidationError, ValidatorOptions} from 'class-validator';
import {BadRequestError} from '@errors';
import {plainToClass} from 'class-transformer';
import {RequestTypes} from '@interfaces';

export default (model: new (...args: any[]) => any, classValidatorOptions?: ValidatorOptions) => {
    return async (ctx: RequestTypes.Context, next: RequestTypes.NextMiddleware): Promise<void> => {
        const entity = plainToClass(model, ctx.request.body);
        const errors: ValidationError[] = await validate(entity, classValidatorOptions);

        if (errors.length) {
            throw new BadRequestError(null, buildErrorPayload(errors));
        } else {
            ctx.state.body = entity;
            await next();
        }
    };
};

function buildErrorPayload(errors: ValidationError[], parentProperty?: string): any {
    return errors.reduce((acc: any, curr: ValidationError) => {
        const propertyName: string = parentProperty ? `${parentProperty}.${curr.property}` : curr.property;
        if (curr.constraints) {
            acc[propertyName] = curr.constraints;
        }

        if (curr.children && curr.children.length) {
            acc = {...acc, ...buildErrorPayload(curr.children, propertyName)};
        }

        return acc;
    }, {});
}
