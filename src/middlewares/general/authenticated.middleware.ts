import {appContainer, publicRoutes, TYPES} from '@config';
import {UNAUTHORIZED} from 'http-status-codes';
import {RequestTypes} from '@interfaces';
import {AuthorizationService} from '@services/authorization.service';
const shouldAuthenticate = (url: string): boolean => !publicRoutes.some(p => url.startsWith(p));

export default async (
    ctx: RequestTypes.Context,
    next: RequestTypes.NextMiddleware,
    autorizationService: AuthorizationService
): Promise<void> => {
    const url: string = ctx.request.req.url;
    const headers: any = ctx.request.header;

    if (shouldAuthenticate(url)) {
        ctx.state.authSettings = {
            sub: autorizationService.resumeSession(headers.authorization)?.user?.id
        };
        ctx.state.owner = ctx.state.authSettings.sub;
        ctx.assert(!!ctx.state.owner, UNAUTHORIZED, 'You are not authorized');
    }

    await next();
};
