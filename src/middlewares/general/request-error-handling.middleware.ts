import {INTERNAL_SERVER_ERROR} from 'http-status-codes';
import {RequestTypes} from '@interfaces';

export default async (ctx: RequestTypes.Context, next: RequestTypes.NextMiddleware): Promise<void> => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.statusCode || err.status || INTERNAL_SERVER_ERROR;
    ctx.body = {
      code: ctx.status,
      name: err.name,
      message: err.message,
      payload: err.payload ?? {}
    };
    ctx.state = {
      ...ctx.state,
      error: err
    };

    ctx.app.emit('error', err, ctx);
  }
};
