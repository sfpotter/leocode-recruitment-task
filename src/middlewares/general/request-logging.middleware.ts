import {LOGGER} from '@config';
import {RequestTypes} from '@interfaces';

export default async (ctx: RequestTypes.Context, next: RequestTypes.NextMiddleware): Promise<void> => {
    const requestStart: number = new Date().getMilliseconds();
    await next();
    const requestEnd: number = new Date().getMilliseconds();

    LOGGER.log(getLogLevel(ctx.status), `${ctx.method} ${ctx.originalUrl} ${ctx.status} ${requestEnd - requestStart}ms`, {
        payload: {
            url: ctx.originalUrl,
            method: ctx.method,
            status: ctx.status,
            ...(ctx.state?.error && {
                body: ctx.state?.body ?? ctx.request?.body ?? null,
                queryParams: ctx.state?.queryParams ?? ctx.request?.query ?? null
            })
        },
        ...(ctx.state?.error && {error: ctx.state.error})
    });
};

function getLogLevel(statusCode: number): string {
    if (statusCode >= 400) {
        return 'error';
    }

    return 'info';
}
